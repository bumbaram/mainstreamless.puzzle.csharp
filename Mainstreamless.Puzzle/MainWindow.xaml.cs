﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Mainstreamless.Puzzle
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();

            GetResources();
            this.DataContext = this;                       
        }

        public ObservableCollection<string> Servers { get; set; }
        public int Errors 
        {
            get { return errors; }
            set
            {
                errors = value;
                Notify("Errors");
            }
        }
        public int Requests 
        {
            get { return requests; }
            set
            {
                requests = value;
                Notify("Requests");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void GetResources()
        {
            Servers = new ObservableCollection<string>();
            Servers.Add(@"http://fit.azoft.com/server1");
            Servers.Add(@"http://fit.azoft.com/server2");
            Servers.Add(@"http://fit.azoft.com/server3");
            Servers.Add(@"http://fit.azoft.com/server4");
            Servers.Add(@"http://fit.azoft.com/server5");
        }

        private void Write(string mess)
        {
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                StringBuilder builder = new StringBuilder(txt_Log.Text);
                builder.Append(mess);
                builder.Append("\n");
                txt_Log.Text = builder.ToString();
            }), null);
        }

        
        private void btn_Start_Click(object sender, RoutedEventArgs e)
        {
            btn_Start.IsEnabled = false;

            isComplite = false;

            timer = new DispatcherTimer();
            timer.Tick += timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            seconds = 0;
            timer.Start();

            builder = new PuzzleBuilder(5);
            builder.Complite += builder_Complite;
            
            foreach (var server in Servers)
            {
                Write(String.Format("Start new thread for the server = {0}", server));
                Task.Factory.StartNew(() =>
                    {
                        DowloadElements(server);
                    });
            }
        }

        void builder_Complite(object sender, ImagesCompliteEventArgs e)
        {
            isComplite = true;
            this.Dispatcher.Invoke(new Action(() =>
            {
                btn_Start.IsEnabled = true;
                timer.Stop();
            }), null);

            foreach (var image in e.Images)
            {
                try
                {
                    File.WriteAllBytes(image.Name, image.Data);
                }
                catch (Exception ex)
                {
                    Write(String.Format("File = {0}, was not saved. Exception = {1}", image.Name, ex.Message));
                }
                Write(String.Format("File = {0}, was saved seccsessfully", image.Name));
            }
            Write("Collection is completed");
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            txt_Timer.Text = (seconds++).ToString();
        }


        private bool isComplite = false;
        private int errors = 0;
        private int requests = 0;
        private int seconds = 0;
        private object errorsSync = new object();
        private object requestsSync = new object();
        private DispatcherTimer timer;
        private PuzzleBuilder builder;
        private DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Element));

        private void DowloadElements(string server)
        {
            try
            {                
                while (!isComplite)
                {
                    lock (requestsSync) Requests++;
                                        
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server);
                    HttpWebResponse response = null;
                    try
                    {
                        response = (HttpWebResponse)request.GetResponse();
                    }
                    catch(Exception ex) 
                    {
                        lock (errorsSync) Errors++;
                        Write("Server returns an errror = " + ex.Message);
                        continue; 
                    }

                    using (var stream = response.GetResponseStream())
                    {
                        Element el;
                        try
                        {
                            lock (serializer)
                                el = serializer.ReadObject(stream) as Element;
                        }
                        catch 
                        {
                            lock (errorsSync) Errors++;
                            Write("Object data wasn't deserialized");
                            continue; 
                        }

                        if (el == null) continue;
                        Write("Gotten object was added to process");
                        builder.Elements.Enqueue(el);
                    }
                }
            }
            catch (Exception ex)
            {
                Write("Exception in the thread = " + ex.Message);
            }
        }


        private void Notify(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

    }
}
