﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mainstreamless.Puzzle
{
    class ImagesCompliteEventArgs : EventArgs
    {
        public ImagesCompliteEventArgs(PuzzleImage[] images)
        {
            Images = images;
        }

        public PuzzleImage[] Images { get; private set; }
    }
}
