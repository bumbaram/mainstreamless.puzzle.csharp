﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Mainstreamless.Puzzle
{
    [Serializable, DataContract(Name="Element"), XmlRoot("Element")]
    class Element
    {
        [DataMember(Name="imageName"), XmlElement("imageName")]
        public string Name { get; set; }
        [DataMember(Name="sizeOfPartInBytes"), XmlElement("sizeOfPartInBytes")]
        public int PartSize { get; set; }
        [DataMember(Name="sizeOfImageInBytes"), XmlElement("sizeOfImageInBytes")]
        public int FullSize { get; set; }
        [DataMember(Name="imagePartNumber"), XmlElement("imagePartNumber")]
        public int Number { get; set; }
        [DataMember(Name="base64Data"), XmlElement("base64Data")]
        public string Base64Data { get; set; }


        public byte[] Data
        {
            get
            {
                try
                {
                    return System.Convert.FromBase64String(Base64Data);
                }
                catch { }
                return null;
            }
        }
    }
}
