﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mainstreamless.Puzzle
{
    /// <summary>
    /// To process element, add it to the Elements collection. 
    /// When bulder completes, Comlete event will raised
    /// </summary>
    class PuzzleBuilder
    {
        public PuzzleBuilder(int number)
        {
            images = new PuzzleImage[number];
            Elements = new BlockingQueue<Element>();

            Task.Factory.StartNew(Collecter);            
        }

        public event EventHandler<ImagesCompliteEventArgs> Complite = delegate { };
        
        public BlockingQueue<Element> Elements { get; private set; }
                
        private PuzzleImage[] images;
        
        /// <summary>
        /// Starts a new thread to process recived elements.
        /// </summary>
        private void Collecter()
        {
            bool isDone = false;
            while (!isDone)
            {
                Element el = Elements.Dequeue();
                el.Number = el.Number - 1;
                int num = GetImageNumber(el.Name);

                if (images[num] == null)
                    images[num] = new PuzzleImage(el.Name, el.FullSize, (int)el.FullSize / el.PartSize);

                if (!images[num].IsReady())
                {                    
                    if (el.Number >= images[num].Parts.Length ||
                        images[num].Parts[el.Number])
                        continue;
                    else
                    {
                        el.Data.CopyTo(images[num].Data, el.Number * el.PartSize);
                        images[num].Parts[el.Number] = true;
                    }
                }
                if (CheckAll()) 
                    isDone = true;
            }
            Complite(this, new ImagesCompliteEventArgs(images));      
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Return the number of image or 
        /// the number an empty array element</returns>
        private int GetImageNumber(string name)
        {
            int? buf = null;

            for (int i = 0; i < images.Length; i++)
            {
                if (buf == null && images[i] == null)
                {
                    buf = i;
                    continue;
                }
                if (images[i] != null && images[i].Name == name)
                    return i;
            }
            return (int)buf;
        }


        private bool CheckAll()
        {
            foreach (var image in images)
            {
                if (image == null || !image.IsReady())
                    return false;
            }
            return true;
        }
    }
}
