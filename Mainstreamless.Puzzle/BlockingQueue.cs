﻿using System.Collections.Generic;
using System.Threading;

namespace Mainstreamless.Puzzle
{
    class BlockingQueue<T>
    {
        public void Enqueue(T data)
        {
            lock (queue)
                queue.Enqueue(data);

            sync.Set();
        }

        public T Dequeue()
        {

            if (queue.Count == 0)
            {
                sync.Reset();
            }


            sync.WaitOne();

            lock (queue)
                return queue.Dequeue();
        }


        private Queue<T> queue = new Queue<T>();
        private ManualResetEvent sync = new ManualResetEvent(false);


    }
}
