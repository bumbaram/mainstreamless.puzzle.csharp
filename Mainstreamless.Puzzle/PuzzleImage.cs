﻿using System;

namespace Mainstreamless.Puzzle
{
    class PuzzleImage
    {
        public PuzzleImage(string name, int size, int details)
        {
            Name = name;
            Data = new byte[size];
            Parts = new bool[details];
        }

        public string Name { get; private set; }
        public byte[] Data { get; private set; }
        public bool[] Parts { get; private set; }

        public bool IsReady()
        {
            foreach (var part in Parts)
            {
                if (!part) return false;
            }
            return true;
        }
    }
}
